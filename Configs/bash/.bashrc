# ~/.bashrc: executed by bash(1) for non-login shells.

# If not running interactively, don't do anything
case $- in
  *i*) ;;
    *) return;;
esac

# Source global definitions
if [ -f /etc/bashrc ]; then
  . /etc/bashrc
fi

# disable flow control
stty -ixon

# path to history file
HISTFILE=~/.bash_history

# don't put duplicate lines or lines starting with space in the history.
# erase duplicates.
# See bash(1) for more options
HISTCONTROL=ignoreboth:erasedups

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=2000
HISTFILESIZE=4000

# Append to bash history every 15 minutes - called from PROMPT_COMMAND
_BASH_HIST_LAST_UPDATE="$EPOCHSECONDS"
append_history_periodically() {
  _BASH_NOW="$EPOCHSECONDS"
  _BASH_TIME_DIFF="$((_BASH_NOW - _BASH_HIST_LAST_UPDATE))"

  # append history and reset _BASH_HIST_LAST_UPDATE
  if [ "${_BASH_TIME_DIFF}" -gt 900 ]; then
    history -a
    # history -n
    _BASH_HIST_LAST_UPDATE="${_BASH_NOW}"
  fi

  unset _BASH_NOW
  unset _BASH_TIME_DIFF
}
export PROMPT_COMMAND="append_history_periodically"

if [ -e /run/.containerenv ]; then
  CONTAINER_PS1='\033[34m ⬢ \033[0m'
else
  CONTAINER_PS1=""
fi

# show git info
GITPROMPT=/usr/share/git-core/contrib/completion/git-prompt.sh
if [ -e "$GITPROMPT" ]; then
  export GIT_PS1_SHOWDIRTYSTATE=1
  . "$GITPROMPT"
  GITPS='\033[1;34m$([ -z "$NO_GITPROMPT" ] && __git_ps1 " (%s)")\033[0m'
  alias gitprompt_toggle='[ -z "$NO_GITPROMPT" ] && export NO_GITPROMPT=1 || unset NO_GITPROMPT'
else
  GITPS=""
fi
unset GITPROMPT

# number of stopped jobs
__jobscount() {
  local jobsc=0
  for _ in $(jobs -ps); do ((jobsc++)); done
  if [ "$jobsc" -gt 0 ]; then
    echo -e -n " \033[0;32m(bg:${jobsc})\033[0m"
  fi
}

# running in MC
__inmc() {
  if [ -n "$MC_SID" ]; then
    echo -e -n " \033[0;32mmc\033[0m"
  fi
}

# running in nix shell
__innix() {
  if [ -n "$IN_NIX_SHELL" ]; then
    echo -e -n "\033[0;34m ❆\033[0m"
  fi
}

NETNS="$(ip netns identify $$)"
if [ -n "$NETNS" ]; then
  NS_PS1=" \033[0;32mns:$NETNS\033[0m"
else
  NS_PS1=""
fi

TIME_PS1=' \D{%m%d-%H:%M:%S}'

# color prompt
if [ "$UID" -ne 0 ]; then
  PS1='\033[1;34m\u\033[0m@\033[0;32m\h\033[0m:\033[0;33m\w\033[0m'"${GITPS}${NS_PS1}"'$(__inmc; __jobscount)'"${TIME_PS1}"'$(__innix;)'"${CONTAINER_PS1}"'\n$ '
else
  # root shell
  PS1='\033[1;31m\u\033[0m@\033[0;32m\h\033[0m:\033[0;33m\w\033[0m'"$NS_PS1"'$(__inmc; __jobscount)'"${TIME_PS1}"'$(__innix;)'"${CONTAINER_PS1}"'\n# '
fi
unset GITPS
unset CONTAINER_PS1
# simple prompt
#PS1='\u@\h:\w\$ '

# if this is an xterm or screen/tmux set the title to user@host:dir
case "$TERM" in
  xterm*|rxvt*|screen*)
    PS1="\[\e]0;\u@\h: \w\a\]$PS1"
    ;;
esac

# additional settings and aliases
if [ -e ~/.bash_aliases ]; then
  . ~/.bash_aliases
fi

# environment variables
if [ -e ~/.env.sh ]; then
  . ~/.env.sh
fi
