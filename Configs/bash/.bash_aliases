#!/bin/bash
# vim:foldmethod=marker:foldlevel=0

shopt -s expand_aliases

if command -v eza >/dev/null 2>&1; then
  alias eza='eza --icons=auto --color=auto'
  alias la='eza -a -1'
  alias lt='eza -sold -1'
  alias ll='eza -a -l'
  alias lg='eza --git-ignore --git -a -1'
else
  alias ls='ls --color=auto -CF'
  alias la='ls -1 -A'
  alias lt='ls -1 -t'
  alias ll='ls -l -t'
fi

alias lsperm='stat -c "%a %n"'
alias dua='du -ahxd1 |sort -h -r'
alias gdiff='git diff --color-words --no-index'

alias sshnull='ssh -A -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no'
alias scpnull='scp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no'

alias nopyc='export PYTHONDONTWRITEBYTECODE=x'

pushd() {
  if [ "$#" -eq 0 ]; then
    DIR="$HOME"
  else
    DIR="$1"
  fi

  builtin pushd "${DIR}" > /dev/null || return 1
  echo -n "DIRSTACK: "
  dirs
}


# editor / vim {{{
if command -v nvim >/dev/null 2>&1; then
  export EDITOR=nvim
elif command -v vimx >/dev/null 2>&1; then
  export EDITOR=vimx
elif command -v vim >/dev/null 2>&1; then
  export EDITOR=vim
else
  export EDITOR=vi
fi
export VISUAL="$EDITOR"
# shellcheck disable=SC2139
alias vim="$EDITOR"
# }}}


# bat {{{
if command -v bat >/dev/null 2>&1; then
  export VIEWER=batviewer
fi
# }}}


# mc {{{

# mc skin
if [ -e /usr/share/mc/skins/modarin256.ini ]; then
  export MC_SKIN=/usr/share/mc/skins/modarin256.ini
fi

# mc workaround for podman - TODO - remove
alias mc='DBUS_SYSTEM_BUS_ADDRESS=unix:path=/nonexistent . ~/bin/mc-wrapper.sh'
# }}}


# tmux / tmuxp {{{
alias mux='tmuxp load -y'
alias tmux_update_env='source <(tmux show-env -s | grep -v "SSH_AUTH_SOCK")'
# }}}


# fzf {{{
_FZF_KEY=/usr/share/fzf/shell/key-bindings.bash
if [ -e "$_FZF_KEY" ]; then
  # shellcheck disable=SC1090
  . "$_FZF_KEY"
fi
unset _FZF_KEY

if command -v rg >/dev/null 2>&1; then
  # export FZF_DEFAULT_COMMAND="rg --files --no-ignore-vcs --hidden --follow"
  export FZF_DEFAULT_COMMAND="rg --files --hidden --follow"
  export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
fi
if command -v bat >/dev/null 2>&1; then
  export FZF_CTRL_T_OPTS="--ansi --preview-window 'right:50%' --preview \
    'bat --color=always --style=header,grid --line-range :200 {}'"
fi
if command -v eza >/dev/null 2>&1; then
  export FZF_ALT_C_OPTS="--preview 'eza --tree --color=always --icons=auto {} | head -200'"
fi

_FZF_GIT=~/.source-fzf-git.sh
if [ -e "$_FZF_GIT" ]; then
  #  CTRL-G CTRL-F for Files
  #  CTRL-G CTRL-B for Branches
  #  CTRL-G CTRL-T for Tags
  #  CTRL-G CTRL-R for Remotes
  #  CTRL-G CTRL-H for commit Hashes
  #  CTRL-G CTRL-S for Stashes
  #  CTRL-G CTRL-L for reflogs
  #  CTRL-G CTRL-W for Worktrees
  #  CTRL-G CTRL-E for Each ref (git for-each-ref)
  # shellcheck disable=SC1090
  . "$_FZF_GIT"
fi
unset _FZF_GIT
# }}}


# zoxide {{{
if command -v zoxide >/dev/null 2>&1; then
  eval "$(zoxide init bash)"
fi
# }}}


# cheat {{{
alias ch='cheat.sh'
complete -A command cheat.sh
complete -A command ch
# }}}


# tldr {{{
complete -A command tldr
# }}}


# sgpt {{{
# integration BASH v0.1
if command -v sgpt >/dev/null 2>&1; then
    _sgpt_bash() {
        if [[ -n "$READLINE_LINE" ]]; then
            READLINE_LINE=$(sgpt --shell <<< "$READLINE_LINE")
            READLINE_POINT=${#READLINE_LINE}
        fi
    }
    bind -x '"\C-l": _sgpt_bash'
fi
# }}}


# ast-grep {{{
if command -v ast-grep >/dev/null 2>&1; then
  # shellcheck disable=SC1090
  source <(ast-grep completions)
fi
# }}}
