# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.

if [ -z "$PATH_INITIALIZED" ]; then
  export PATH_INITIALIZED=1
  PATH="/usr/local/bin:/usr/local/sbin:/usr/bin:/usr/sbin"
  if [ ! -L /bin ] && [ -d /bin ]; then
    PATH="$PATH:/bin"
  fi
  if [ ! -L /sbin ] && [ -d /sbin ]; then
    PATH="$PATH:/sbin"
  fi
  export PATH
fi

pathmunge() {
  [ -d "$1" ] || return 1
  case "${PATH}" in
    *:${1}:*|${1}:*|*:${1}|${1}) ;;
    *)
      if [ "$2" = "after" ]; then
        PATH=${PATH}:$1
      else
        PATH=${1}:$PATH
      fi ;;
  esac
  return 0
}

# ulimits
if hash ulimit; then
  ulimit -n 400000  # open files
  ulimit -u 50000   # max user processes
  export MLIMITS_SET=true
fi

# locale settings
export LC_MESSAGES=en_US.UTF-8
export LANG=cs_CZ.UTF-8

# set PATH so it includes user's $PYTHONUSERBASE/bin if it exists
if pathmunge "$HOME/.local/python_user/bin"; then
  export PYTHONUSERBASE="$HOME/.local/python_user"
fi

# set PATH so it includes user's private bin if it exists
pathmunge "$HOME/bin"

# set PATH so it includes user's .local/bin if it exists
pathmunge "$HOME/.local/bin"

# set PATH so it includes user's .gem/bin if it exists
pathmunge "$HOME/.gem/bin"

# set PATH so it includes user's node_modules/.bin if it exists
pathmunge "$HOME/.npm-global/bin"

# set PATH so it includes user's .cargo/bin if it exists
pathmunge "$HOME/.cargo/bin"

# set PATH so it includes user's $GOPATH/bin if it exists
pathmunge "$HOME/go/bin"

# set PATH so it includes user's .cabal/bin if it exists
pathmunge "$HOME/.ghcup/bin"

# set PATH so it includes user's .cabal/bin if it exists
pathmunge "$HOME/.cabal/bin"

# settings when running inside podman
if [ -e "$HOME/.profile_podman" ]; then
  . "$HOME/.profile_podman"
fi

unset -f pathmunge
export PATH

# Nix
if [ -z "$NIX_PROFILES" ] && [ -d /nix/store ]; then
  if [ -e "$HOME/.nix-profile/etc/profile.d/nix.sh" ]; then
    . "$HOME/.nix-profile/etc/profile.d/nix.sh"
  fi
  if [ -e "$HOME/.nix-profile/etc/profile.d/hm-session-vars.sh" ]; then
    . "$HOME/.nix-profile/etc/profile.d/hm-session-vars.sh"
  fi
  export XDG_DATA_DIRS=$HOME/.nix-profile/share:"${XDG_DATA_DIRS:-/usr/local/share/:/usr/share/}"
  LOCALE_ARCHIVE=/usr/lib/locale/locale-archive
  if [ ! -e "$LOCALE_ARCHIVE" ]; then
    LOCALE_ARCHIVE="$(nix-build --no-out-link "<nixpkgs>" -A glibcLocales)/lib/locale/locale-archive"
  fi
  export LOCALE_ARCHIVE
fi

# source .bashrc if running bash
if [ -n "$BASH_VERSION" ] && [ -f "$HOME/.bashrc" ]; then
  . "$HOME/.bashrc"
fi
