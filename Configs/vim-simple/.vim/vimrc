" Initial config {{{
" vim:foldmethod=marker:foldlevel=0

" character encoding used inside Vim
"scriptencoding utf-8
"set encoding=utf-8

set nocompatible

" remap <leader> and <localleader> to ,
let mapleader = ','
let maplocalleader = ','

" highlight extra whitespace
" MUST be inserted BEFORE the colorscheme command
augroup colorsgrp
  autocmd!
  au ColorScheme * highlight ExtraWhitespace ctermbg=red guibg=red
  au InsertLeave * match ExtraWhitespace /\s\+$/
augroup END


"
" rg/grep
"
" use ripgrep over grep
if executable('rg')
  set grepprg=rg\ --vimgrep
  set grepformat^=%f:%l:%c:%m " file:line:column:message
endif


"
" matchit
"
runtime macros/matchit.vim


"
" vim-markdown (included in vim by default)
"
" enable folding
let g:markdown_folding = 1

" }}}


" Sets {{{
" https://github.com/tpope/vim-sensible/blob/master/plugin/sensible.vim

"
" colors
"

" MUST be inserted BEFORE the colorscheme command
augroup colorsgrp
  autocmd!
  " highlight extra whitespace
  au ColorScheme * highlight ExtraWhitespace ctermbg=red guibg=red
  au InsertLeave * match ExtraWhitespace /\s\+$/
augroup END

" fix for running in tmux with :set termguicolors
if !has('nvim') && &term =~# '^\(tmux\|screen\)'
  let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
  let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
endif

if has('gui') || (has('termguicolors') && (($COLORTERM ==# 'truecolor') || (stridx($TERM, '256color') >= 0)))
  set termguicolors
endif

syntax enable
if &term=~'linux'
  set background=dark
else
  "set t_Co=256
  if strftime('%H') >= 20 || strftime('%H') < 6
    set background=dark
  else
    set background=light
  endif
  colorscheme gruvbox
endif

set history=1000 " mum of entries remembered for ':' commands and search patterns
set showcmd " show that vim is waiting for key
set scrolloff=3 " min number of lines to keep above and below the cursor
set sidescrolloff=5 " min number of columns to keep to sides
set backspace=indent,eol,start " backspace for dummies
"set number relativenumber  " relative line numbers
"set mouse=a " enable mouse for all modes
set hidden " switch files without saving them first
set clipboard=unnamedplus " use + register for copy-paste
set switchbuf=split " use new split
set lazyredraw " redraw only when we need to
"set completeopt-=preview " turn off the preview window
set ruler " show the line and column number of the cursor position
set autoread " automatically read again changed files
set timeoutlen=800 " mapping delays
set tabpagemax=50 " maximum number of tab pages
set display+=lastline " display as much as possible of the last line
set formatoptions-=t  " don't auto-wrap text using textwidth
set formatoptions-=o  " don't insert the current comment leader to new line
set formatoptions+=j " delete comment character when joining commented lines
set nrformats-=octal " numbers starting with zero will NOT be considered octal
set shortmess+=c " silence "match 1 of 2" etc. messages
set isfname-=:  " not using drive letters, use : as line number separator
set updatetime=500  " longer updatetime (default 4000 ms) leads to noticeable delays
set foldlevelstart=99  " start edit with all folds open
set backupskip+=*.asc  " don't create backup file for asc files

" splits
set splitbelow " new horizontal split to bottom
set splitright " new vertical split to right
set wmh=0 " don't show one line of minimized split

" indentation
set autoindent " copy indent from current line when starting a new line
set smarttab " insert configured number of blanks on <Tab>
set tabstop=4 shiftwidth=4 softtabstop=4 expandtab " use spaces instead of tabs
set list lcs=tab:\|\ " highlight tabs indentation

" sessions
set ssop-=options " do not store global and local values in a session
set ssop-=folds " do not store folds in a session

" search
set incsearch " search as characters are entered
set hlsearch " highlight search terms
" use <C-L> to clear the highlighting of :set hlsearch.
if maparg('<C-L>', 'n') ==# ''
  nnoremap <silent> <C-L> :nohlsearch<C-R>=has('diff')?'<Bar>diffupdate':''<CR><CR><C-L>
endif

" don't look any further for tags if ./tags is present
if has('path_extra')
  setglobal tags-=./tags tags-=./tags; tags^=./tags;
endif

" create viminfo if missing
if !empty(&viminfo)
  set viminfo^=!
endif

" bash-like tab completion
set wildmode=list:longest,full
set wildmenu
set wildignore=*~,*.bak,*.o,*.aut,*.dvi

" don't save backup files
set nobackup
set nowritebackup

" highlight matching bracket
set showmatch
set matchpairs=(:),[:],{:},<:>

" }}}


" Commands {{{

" Rmw = remove trailing whitespaces and ^M chars
command! Rmw %s/\s\+$//e

" spellchecking in current buffer
command! SpellEN :setlocal spell spelllang=en_us
command! SpellOff :setlocal nospell

" run command and display its output in new split
function! s:run_cmd_in_new(to_run, bang) abort
  new
  setlocal buftype=nofile bufhidden=hide noswapfile nowrap
  exe '0r !' . a:to_run
  if !a:bang
    filetype detect
  endif
endfunction
command! -nargs=+ -complete=shellcmd -bang RunInNew :call s:run_cmd_in_new(<q-args>, <bang>0)

" select a jump in the list
function! s:go_to_jump() abort
  jumps
  let j = input('Please select your jump: ')
  if j != ''
    let pattern = '\v\c^\+'
    if j =~ pattern
      let j = substitute(j, pattern, '', 'g')
      exe "normal " . j . "\<c-i>"
    else
      exe "normal " . j . "\<c-o>"
    endif
  endif
endfunction
command! Jump :call s:go_to_jump()

" reload syntax
command! SyntaxSync :syntax sync fromstart

" cd to dir vim was started from
let g:my_startup_dir = expand('%:p:h')
command! Cdstart :exe 'lcd ' . g:my_startup_dir

" format XML
command! -range=% XMLpretty <line1>,<line2>!python3 -c 'import xml.dom.minidom, sys; print(xml.dom.minidom.parse(sys.stdin).toprettyxml())'

" format JSON
command! -range=% JSONpretty <line1>,<line2>!python3 -m json.tool

" format using prettier
command! -range=% Pretty <line1>,<line2>!prettier --stdin-filepath %

" }}}


" Autocommands {{{

function! s:SetColorColumn()
  " if 'colorcolumn' is already set and 'textwidth' is also set, set 'colorcolumn' to a value based on 'textwidth'
  if &colorcolumn != "" && &textwidth > 0
      setlocal colorcolumn=+1
  endif
endfunction

augroup configgroup
  autocmd!

  " vertical bar after 100th character by default, folding
  au FileType c,cpp,go,haskell,java,javascript,lua,python,ruby,rust,sh,typescript setlocal colorcolumn=101 signcolumn=yes foldmethod=indent foldnestmax=2

  " vertical bar based on 'textwidth', if set by e.g. editorconfig
  au BufEnter * call s:SetColorColumn()

  " prevent vim from indenting line when ':' is entered (e.g. type hints in function definition)
  au FileType python setlocal indentkeys-=<:>

  " indentation
  au FileType go,make setlocal noexpandtab " use real tabs for Makefiles and go
  au FileType haskell,lua,ruby,sh,vim,yaml,markdown,rst setlocal expandtab tabstop=2 shiftwidth=2 softtabstop=2

  " override 'switchbuf' for quickfix windows
  au FileType qf setlocal switchbuf=useopen

  " word wrapping for text files
  au FileType markdown,rst,tex,text setlocal wrap linebreak nolist textwidth=0 wrapmargin=0 formatoptions+=t

  " disable syntax concealing for markdown
  au FileType markdown setlocal conceallevel=0

  " folding using syntax
  au FileType html,json,xml setlocal foldmethod=syntax

  " restore last position in file when reopened
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif

  " swap to the last viewed tab
  " http://stackoverflow.com/questions/2119754/switch-to-last-active-tab-in-vim
  au TabLeave * let g:lasttab = tabpagenr()
  let g:lasttab = 1
  nmap <M-t><M-t> :exe 'tabn '.g:lasttab<CR>
  imap <M-t><M-t> <ESC>:exe 'tabn '.g:lasttab<CR>

  " open the location/quickfix window after :make, :grep, :lvimgrep and friends
  " if there are valid locations/errors
  au QuickFixCmdPost [^l]* cwindow
  au QuickFixCmdPost l*    lwindow

  " encrypt/decrypt files
  au BufReadPost  *.asc :%!gpg -q -d
  au BufReadPost  *.asc |redraw!
  au BufWritePre  *.asc :%!gpg -q --symmetric --armor
  au BufWritePost *.asc u
  au VimLeave     *.asc :!clear
augroup END

" }}}


" Key mappings {{{

" fix Alt key in console
" for mapped keys
if !(has('gui_running') || has('nvim'))
  set <M-j>=j
  set <M-k>=k
  set <M-l>=l
  set <M-h>=h
  set <M-t>=t
endif

" map <Esc> to exit terminal-mode
if has('nvim')
  tnoremap <Esc> <C-\><C-n>
endif

" netrw
nnoremap - :Explore<CR>

" split navigations
nnoremap <M-h> <C-W><C-H>
nnoremap <M-j> <C-W><C-J>
nnoremap <M-k> <C-W><C-K>
nnoremap <M-l> <C-W><C-L>

" moving lines up and down
inoremap <M-j> <Esc>:m .+1<CR>==gi
inoremap <M-k> <Esc>:m .-2<CR>==gi
vnoremap <M-j> :m '>+1<CR>gv=gv
vnoremap <M-k> :m '<-2<CR>gv=gv

" move tab to right
nmap <M-t><M-l> :tabm +1<CR>
" move tab to left
nmap <M-t><M-h> :tabm -1<CR>

" new tab
nnoremap <leader>tt :$tabnew<CR>

" sane indentation on paste
noremap <leader>pt :set invpaste<CR>

" visual shifting (does not exit Visual mode)
vnoremap < <gv
vnoremap > >gv

" search for visually selected text
vnoremap // y/<C-R>"<CR>

" highlight word without jumping
nnoremap * *``

" move vertically by visual line
nnoremap j gj
nnoremap k gk

" highlight last inserted text
nnoremap gV `[v`]

" allow using the repeat operator with a visual selection (!)
" http://stackoverflow.com/a/8064607/127816
vnoremap . :normal .<CR>

" recoverable undo
inoremap <c-u> <c-g>u<c-u>
inoremap <c-w> <c-g>u<c-w>

" for when you forget to sudo.. Really Write the file.
cmap w!! w !sudo tee % >/dev/null

" change Working Directory to that of the current file
cmap cd. lcd %:p:h

" }}}
