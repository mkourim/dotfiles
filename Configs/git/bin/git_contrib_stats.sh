#!/bin/bash

# e.g.
# --since="15 Jan 2015" --before="25 Oct 2019"

author=mkourim@redhat.com
if [[ "$1" == *"@"* ]]; then
  author="$1"
  shift
fi

git log --author="$author" --pretty=tformat: --numstat "$@" | gawk '{ add += $1; subs += $2; loc += $1 + $2 } END { printf "added lines: %s removed lines: %s total lines: %s\n", add, subs, loc }' -
