#!/bin/sh

sname="${1:?"Pass socket name"}"
tmux -L "$sname" attach || tmux -L "$sname" new-session -s "$sname"
