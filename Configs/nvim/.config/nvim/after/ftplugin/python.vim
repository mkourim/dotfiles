" send word under the cursor to tmux pane (depends on vim-slime)
nnoremap <leader>rr :SlimeSend1 <C-r><C-w><CR>

" prevent vim from indenting line when ':' is entered (e.g. type hints in function definition)
setlocal indentkeys-=<:>
