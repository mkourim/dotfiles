-- vim:foldmethod=marker:foldlevel=0

-- lazy.nvim {{{
local lazypath = vim.fn.stdpath('data') .. '/lazy/lazy.nvim'
if not (vim.uv or vim.loop).fs_stat(lazypath) then
  vim.fn.system({
    'git',
    'clone',
    '--filter=blob:none',
    'https://github.com/folke/lazy.nvim.git',
    '--branch=stable', -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)
-- }}}

-- Cool plugins for later
-- ----------------------
-- https://github.com/rockerBOO/awesome-neovim - A curated list of awesome Neovim plugins and resources.
-- rest-nvim/rest.nvim - REST client for Neovim.
-- levouh/tint.nvim - Dim inactive splits. Currently handled by gruvbox.

-- backup lazy-lock.json {{{
-- https://dev.to/vonheikemen/lazynvim-how-to-revert-a-plugin-back-to-a-previous-version-1pdp
local lazy_cmds = vim.api.nvim_create_augroup('lazy_cmds', {clear = true})
local snapshot_dir = vim.fn.stdpath('data') .. '/plugin-snapshot'
local lockfile = vim.fn.stdpath('config') .. '/lazy-lock.json'

vim.api.nvim_create_user_command(
  'LazyBrowseSnapshots',
  'edit ' .. snapshot_dir,
  {}
)

vim.api.nvim_create_autocmd('User', {
  group = lazy_cmds,
  pattern = 'LazyUpdatePre',
  desc = 'Backup lazy.nvim lockfile',
  callback = function(event)
    vim.fn.mkdir(snapshot_dir, 'p')
    local snapshot = snapshot_dir .. os.date('/%Y-%m-%dT%H:%M:%S.json')

    vim.loop.fs_copyfile(lockfile, snapshot)
  end,
})
-- }}}

require('lazy').setup({
  'LnL7/vim-nix',
  {'nvim-lua/plenary.nvim', lazy = true },
  {'nvim-lua/popup.nvim', lazy = true },

  -- nvim-tree/nvim-web-devicons {{{
  {
    'nvim-tree/nvim-web-devicons',
    opts = {
      -- globally enable different highlight colors per icon (default to true)
      -- if set to false all icons will have the default icon's color
      color_icons = true;
      -- globally enable default icons (default to false)
      -- will get overriden by `get_icons` option
      default = true;
    },
  },
  -- }}}

  -- justinmk/vim-sneak {{{
  {
    'justinmk/vim-sneak',
    event = { 'BufReadPre', 'BufNewFile' },
  },
  -- }}}

  -- tpope/vim-repeat {{{
  {
    'tpope/vim-repeat',
    event = { 'BufReadPre', 'BufNewFile' },
  },
  -- }}}

  -- tpope/vim-surround {{{
  {
    'tpope/vim-surround',
    event = { 'BufReadPre', 'BufNewFile' },
  },
  -- }}}

  -- folke/which-key.nvim {{{
  {
    'folke/which-key.nvim',
    event = 'VeryLazy',
    config = function()
      -- vim.o.timeout = true
      -- vim.o.timeoutlen = 300
      local wk = require('which-key')
      wk.setup()
      wk.add({
        { '<leader>h', group = 'Diff hunks' },
        { '<leader>g', group = 'Grep' },
        { '<space>i', group = 'LSP Diagnostic' },
        { '<space>w', group = 'LSP Workspace' },
      })
      vim.keymap.set('n', '<leader>?', function()
        wk.show({ global = false })
      end, { desc = 'Buffer Local Keymaps (which-key)' })
    end,
  },
  -- }}}

  -- ellisonleao/gruvbox.nvim {{{
  {
    'ellisonleao/gruvbox.nvim',
    config = function()
      require('gruvbox').setup {
        dim_inactive = true, -- dim inactive splits
      }
      -- load the colorscheme here
      vim.cmd([[colorscheme gruvbox]])
    end,
  },
  -- }}}

  -- nvim-lualine/lualine.nvim {{{
  {
    'nvim-lualine/lualine.nvim',
    opts = {
      options = { theme = 'gruvbox' },
      tabline = {
        lualine_a = {
          {
            'tabs',
            mode = 2, -- 2: Shows tab_nr + tab_name
            max_length = vim.o.columns,
            -- Automatically updates active tab color to match color of other components (will be overidden if buffers_color is set)
            use_mode_colors = true,
          }
        }
      }
    },
  },
  -- }}}

  -- nvim-treesitter/nvim-treesitter {{{
  {
    'nvim-treesitter/nvim-treesitter',
    dependencies = {
      'nvim-treesitter/nvim-treesitter-context',
    },
    build = ':TSUpdate',

    config = function()
      require('nvim-treesitter.configs').setup {
        -- A list of parser names, or "all" (the listed parsers should always be installed)
        ensure_installed = {
          'bash',
          'c',
          'css',
          'diff',
          'go',
          'haskell',
          'html',
          'javascript',
          'json',
          'lua',
          'markdown',
          'nix',
          'python',
          'query',
          'regex',
          'rust',
          'rst',
          'toml',
          'typescript',
          'vim',
          'vimdoc',
          'yaml' },

        -- Install parsers synchronously (only applied to `ensure_installed`)
        sync_install = false,

        -- Automatically install missing parsers when entering buffer
        -- Recommendation: set to false if you don't have `tree-sitter` CLI installed locally
        auto_install = false,

        ---- If you need to change the installation directory of the parsers (see -> Advanced Setup)
        -- parser_install_dir = '/some/path/to/store/parsers', -- Remember to run vim.opt.runtimepath:append('/some/path/to/store/parsers')!

        highlight = {
          enable = true,
          -- Or use a function for more flexibility, e.g. to disable slow treesitter highlight for large files
          disable = function(lang, buf)
            local max_filesize = 300 * 1024 -- 300 KB
            local ok, stats = pcall(vim.loop.fs_stat, vim.api.nvim_buf_get_name(buf))
            if ok and stats and stats.size > max_filesize then
              return true
            end
          end,

          -- Setting this to true will run `:h syntax` and tree-sitter at the same time.
          -- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
          -- Using this option may slow down your editor, and you may see some duplicate highlights.
          -- Instead of true it can also be a list of languages
          additional_vim_regex_highlighting = false,
        },
      }

      -- treesitter-context {{{
      require('treesitter-context').setup {
        max_lines = 4, -- How many lines the context should span. Values <= 0 mean no limit.
        min_window_height = 40, -- Minimum editor window height to enable context. Values <= 0 mean no limit.
        multiline_threshold = 1, -- Maximum number of lines to show for a single context
      }
      -- }}}
    end,
  },
  -- }}}

  -- nvim-telescope/telescope.nvim {{{
  {
    'nvim-telescope/telescope.nvim',
    branch = '0.1.x',
    dependencies = {
      'nvim-telescope/telescope-ui-select.nvim',
      { 'nvim-telescope/telescope-fzf-native.nvim', build = 'make' },
      'nvim-telescope/telescope-live-grep-args.nvim',
      'debugloop/telescope-undo.nvim',
      'jvgrootveld/telescope-zoxide',
    },
    config = function()
      local telescope = require('telescope')
      local actions = require('telescope.actions')
      local builtin = require('telescope.builtin')
      local live_grep_args = require('telescope-live-grep-args.shortcuts')

      vim.keymap.set('n', '<C-t>', builtin.find_files, { noremap = true, silent = true, desc = 'Fuzzy find files' })
      vim.keymap.set('n', '<leader>ww', builtin.buffers, { noremap = true, silent = true, desc = 'Find buffers' })
      vim.keymap.set('n', '<leader>gr', live_grep_args.grep_word_under_cursor, { noremap = true, silent = true, desc = 'Grep word under cursor' })
      vim.keymap.set('n', '<leader>gg', telescope.extensions.live_grep_args.live_grep_args, { noremap = true, silent = true, desc = 'Live ripgrep with telescope' })

      vim.keymap.set('n', '<leader>ff', function()
        builtin.current_buffer_fuzzy_find({ default_text = vim.fn.expand('<cword>') })
      end, { noremap = true, silent = true, desc = 'Find keyword under cursor in buffer' })

      -- search symbols in current file using LSP
      vim.keymap.set('n', '<space>d', builtin.lsp_document_symbols, { noremap = true, silent = true, desc = 'LSP Document symbols' })
      vim.keymap.set('n', '<space>D', builtin.lsp_type_definitions, { noremap = true, silent = true, desc = 'LSP Type definitions' })
      vim.keymap.set('n', 'gd', builtin.lsp_definitions, { noremap = true, silent = true, desc = 'LSP Definitions' })
      vim.keymap.set('n', 'gr', builtin.lsp_references, { noremap = true, silent = true, desc = 'LSP References' })
      vim.keymap.set('n', 'gi', builtin.lsp_implementations, { noremap = true, silent = true, desc = 'LSP Implementations' })

      vim.api.nvim_create_user_command('Toldfiles', builtin.oldfiles, { bang = true, nargs = '*', desc = 'Find old files' })
      vim.api.nvim_create_user_command('Z', telescope.extensions.zoxide.list, { bang = true, nargs = '*', desc = 'Zoxide list' })

      -- use ripgrep over grep
      vim.opt.grepprg = 'rg --vimgrep --smart-case --hidden'
      vim.opt.grepformat:prepend('%f:%l:%c:%m')

      -- grep / Ggrep and display results in Telescope quickfix
      vim.api.nvim_create_user_command('GR', function(opts)
        vim.cmd('silent ' .. (opts.bang and 'grep!' or 'Ggrep!') .. ' ' .. opts.args .. ' | Telescope quickfix')
      end, { bang = true, nargs = '+', desc = 'Grep with Telescope quickfix' })

      -- Show filename first in telescope results
      -- https://github.com/nvim-telescope/telescope.nvim/issues/2014#issuecomment-1873547633
      local function filename_first(_, path)
        local tail = vim.fs.basename(path)
        local parent = vim.fs.dirname(path)
        if parent == '.' then
          return tail
        end
        return string.format('%s\t\t%s', tail, parent)
      end

      vim.api.nvim_create_autocmd('FileType', {
        pattern = 'TelescopeResults',
        callback = function(ctx)
          vim.api.nvim_buf_call(ctx.buf, function()
            vim.fn.matchadd('TelescopeParent', '\t\t.*$')
            vim.api.nvim_set_hl(0, 'TelescopeParent', { link = 'Comment' })
          end)
        end,
      })

      telescope.setup {
        defaults = {
          dynamic_preview_title = true,  -- show file path in preview title
          path_display = filename_first,
        },
        pickers = {
          find_files = {
            hidden = true,
          },
        },
        extensions = {
          fzf = {
            fuzzy = true,                    -- false will only do exact matching
            override_generic_sorter = true,  -- override the generic sorter
            override_file_sorter = true,     -- override the file sorter
            case_mode = 'smart_case',        -- or "ignore_case" or "respect_case"
                                             -- the default case_mode is "smart_case"
          },
          undo = {
            saved_only = false,
          },
          ['ui-select'] = {
            require('telescope.themes').get_dropdown {},
          },
          zoxide = {
            mappings = {
              default = {
                keepinsert = true,
                action = function(selection)
                  vim.cmd.lcd(selection.path)
                  builtin.find_files({ cwd = selection.path })
                end,
                after_action = function(selection)
                  vim.notify('Directory changed to ' .. selection.path)
                end,
              },
              ['<C-l>'] = {
                action = function(selection)
                  vim.cmd.lcd(selection.path)
                end,
                after_action = function(selection)
                  vim.notify('Directory changed to ' .. selection.path)
                end,
              },
            },
          },
        }
      }

      -- Load the extensions. Needs to be loaded *after* telescope setup.
      telescope.load_extension 'ui-select'
      telescope.load_extension 'undo'
      telescope.load_extension 'zoxide'
    end,
  },
  -- }}}

  -- zbirenbaum/copilot.lua {{{
  {
    'zbirenbaum/copilot.lua',
    event = { 'BufReadPre', 'BufNewFile' },
    opts = {
      filetypes = {
        yaml = true,
        markdown = true,
      },
    },
  },
  -- }}}

  -- CopilotC-Nvim/CopilotChat.nvim {{{
  {
    'CopilotC-Nvim/CopilotChat.nvim',
    dependencies = {
      { 'zbirenbaum/copilot.lua' },
    },
    event = { 'BufReadPre', 'BufNewFile' },
    build = 'make tiktoken',
    opts = {
      -- debug = true, -- Enable debugging
      prompts = {
        -- No LSP warnings: https://github.com/CopilotC-Nvim/CopilotChat.nvim/issues/362
        Review = {
          callback = function(_, _)
            -- do nothing
          end,
        },
      },
    },
  },
  -- }}}

  -- jpalardy/vim-slime {{{
  {
    'jpalardy/vim-slime',
    event = { 'BufReadPre', 'BufNewFile' },
    config = function()
      vim.g.slime_target = 'tmux'
      vim.g.slime_default_config = { socket_name = 'default', target_pane = ':.1' }
      vim.g.slime_python_ipython = 1  -- use ipython for python
      vim.g.slime_bracketed_paste = 1  -- enable bracketed paste (better than `slime_python_ipython` when available)
      vim.g.slime_haskell_ghci_add_let = 0  -- don't add 'let' to ghci commands
    end,
  },
  -- }}}

  -- lukas-reineke/indent-blankline.nvim {{{
  {
    'lukas-reineke/indent-blankline.nvim',
    event = { 'BufReadPre', 'BufNewFile' },
    main = 'ibl',
    dependencies = 'nvim-treesitter/nvim-treesitter',
    opts = {
      scope = { enabled = false },
    },
  },
  -- }}}

  -- numToStr/Comment.nvim {{{
  {
    'numToStr/Comment.nvim',
    event = { 'BufReadPre', 'BufNewFile' },
  },
  -- }}}

  -- tyru/current-func-info.vim {{{
  {
    'tyru/current-func-info.vim',
    ft = 'python'
  },
  -- }}}

  -- lewis6991/gitsigns.nvim {{{
  {
    'lewis6991/gitsigns.nvim',
    event = { 'BufReadPre', 'BufNewFile' },
    config = function()
      require('gitsigns').setup{
        on_attach = function(bufnr)
          local gitsigns = require('gitsigns')

          local function map(mode, l, r, opts)
            opts = opts or {}
            opts.buffer = bufnr
            vim.keymap.set(mode, l, r, opts)
          end

          -- Navigation
          map('n', ']c', function()
            if vim.wo.diff then
              vim.cmd.normal({']c', bang = true})
            else
              gitsigns.nav_hunk('next')
            end
          end, { desc = 'Next hunk' })

          map('n', '[c', function()
            if vim.wo.diff then
              vim.cmd.normal({'[c', bang = true})
            else
              gitsigns.nav_hunk('prev')
            end
          end, { desc = 'Previous hunk' })

          -- Actions
          map('n', '<leader>hs', gitsigns.stage_hunk, { desc = 'Stage hunk' })
          map('n', '<leader>hr', gitsigns.reset_hunk, { desc = 'Reset hunk' })
          map('v', '<leader>hs', function()
            gitsigns.stage_hunk {vim.fn.line('.'), vim.fn.line('v')}
          end, { desc = 'Stage hunk' })
          map('v', '<leader>hr', function()
            gitsigns.reset_hunk {vim.fn.line('.'), vim.fn.line('v')}
          end, { desc = 'Reset hunk' })
          map('n', '<leader>hu', gitsigns.undo_stage_hunk, { desc = 'Undo stage hunk' })
          map('n', '<leader>hd', gitsigns.diffthis, { desc = 'Diff this' })

          -- Text object
          map({'o', 'x'}, 'ih', ':<C-U>Gitsigns select_hunk<CR>')
        end
      }
    end,
  },
  -- }}}

  -- notjedi/nvim-rooter.lua {{{
  {
    'notjedi/nvim-rooter.lua',
    opts = {
      rooter_patterns = { '.git' },
    },
  },
  -- }}}

  -- tpope/vim-fugitive {{{
  {
    'tpope/vim-fugitive',
    event = 'VeryLazy',
    dependencies = {
      'tpope/vim-rhubarb',
      'shumphrey/fugitive-gitlab.vim',
    },
  },
  -- }}}

  -- sindrets/diffview.nvim {{{
  {
    'sindrets/diffview.nvim',
    event = 'VeryLazy',
  },
  -- }}}

  -- mfussenegger/nvim-dap {{{
  {
    'mfussenegger/nvim-dap',
    lazy = true,
  },
  -- }}}

  -- mrcjkb/haskell-tools.nvim {{{
  {
    'mrcjkb/haskell-tools.nvim',
    version = '^3',
    lazy = false,
    dependencies = {
      'mfussenegger/nvim-dap',
    },
  },
  -- }}}

  -- hrsh7th/nvim-cmp {{{
  {
    'hrsh7th/nvim-cmp',
    dependencies = {
      'andersevenrud/cmp-tmux',
      'hrsh7th/cmp-nvim-lsp',
      'hrsh7th/cmp-buffer',
      'hrsh7th/cmp-cmdline',
      'hrsh7th/cmp-nvim-lsp-signature-help',
      'hrsh7th/cmp-nvim-lua',
      'hrsh7th/cmp-path',
      'neovim/nvim-lspconfig',
      'onsails/lspkind.nvim',
      'mrcjkb/haskell-tools.nvim',
      -- vsnip
      'hrsh7th/cmp-vsnip',
      'hrsh7th/vim-vsnip',
      'rafamadriz/friendly-snippets',
    },
    config = function()
      local cmp = require('cmp')
      local lspkind = require('lspkind') -- Add vscode-like pictograms to completion items

      cmp.setup {
        snippet = {
          expand = function(args)
            vim.fn['vsnip#anonymous'](args.body) -- For `vsnip`.
          end,
        },
        window = {
          completion = cmp.config.window.bordered(),
          documentation = cmp.config.window.bordered(),
        },
        mapping = cmp.mapping.preset.insert({
          ['<C-b>'] = cmp.mapping.scroll_docs(-4),
          ['<C-f>'] = cmp.mapping.scroll_docs(4),
          ['<C-Space>'] = cmp.mapping.complete(),
          -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
          ['<CR>'] = cmp.mapping.confirm({ select = false }),
        }),
        sources = cmp.config.sources({
          { name = 'nvim_lsp' },
          { name = 'nvim_lsp_signature_help' },
          { name = 'vsnip' },
          { name = 'nvim_lua' },
        }, {
          { name = 'buffer' },
          { name = 'tmux' },
        }),
        formatting = {
          format = lspkind.cmp_format({ mode = 'symbol_text', maxwidth = 50 }),
        },
      }

      -- Use buffer source for `/` and `?` (if you enabled `native_menu`, this won't work anymore).
      cmp.setup.cmdline({ '/', '?' }, {
        mapping = cmp.mapping.preset.cmdline(),
        sources = {
          { name = 'buffer' }
        }
      })

      -- Use cmdline & path source for ':' (if you enabled `native_menu`, this won't work anymore).
      cmp.setup.cmdline(':', {
        mapping = cmp.mapping.preset.cmdline(),
        sources = cmp.config.sources({
          { name = 'path' }
        }, {
          { name = 'cmdline' }
        })
      })

      -- Set up lspconfig.
      local lspconfig = require('lspconfig')

      -- LSP settings for overriding per client
      local lsp_handlers =  {
        ['textDocument/hover'] =  vim.lsp.with(vim.lsp.handlers.hover, {border = 'rounded'}),
        ['textDocument/signatureHelp'] =  vim.lsp.with(vim.lsp.handlers.signature_help, {border = 'rounded'}),
      }

      if vim.fn.executable('ruff') == 1 then
        lspconfig.ruff.setup {
          handlers = lsp_handlers,
          init_options = {
            settings = {
              organizeImports = true,
              fixAll = true,
              showSyntaxErrors = true
            }
          }
        }
      end

      if vim.fn.executable('pylsp') == 1 then
        lspconfig.pylsp.setup {
          handlers = lsp_handlers,
          settings = {
            pylsp = {
              plugins = {
                mccabe = {enabled = false},
                pycodestyle = {enabled = false},
                pydocstyle = {enabled = false},
                pyflakes = {enabled = false},
                pylint = {enabled= false},
                rope_completion = {enabled = false},
                ruff = {enabled = false},
                yapf = {enabled = false},
                mypy = {enabled = true, live_mode = false, dmypy = true},
                -- jedi_completion = {include_params = false},
              },
              configurationSources = {'flake8', 'pycodestyle'}
            }
          }
        }
      end

      if vim.fn.executable('bash-language-server') == 1 then
        lspconfig.bashls.setup {
          handlers = lsp_handlers
        }
      end

      -- for info on haskell-tools see https://github.com/mrcjkb/haskell-tools.nvim#advanced-configuration

      -- nvim-lspconfig {{{

      -- Global mappings.
      -- See `:help vim.diagnostic.*` for documentation on any of the below functions
      vim.keymap.set('n', '<space>ic', vim.diagnostic.open_float, { desc = 'Open LSP diagnostic float on current issue' })
      vim.keymap.set('n', '<space>il', vim.diagnostic.setloclist, { desc = 'LSP diagnostic loclist' })
      vim.keymap.set('n', ']d', vim.diagnostic.goto_next, { desc = 'Next LSP diagnostic' })
      vim.keymap.set('n', '[d', vim.diagnostic.goto_prev, { desc = 'Previous LSP diagnostic' })

      -- Use LspAttach autocommand to only map the following keys
      -- after the language server attaches to the current buffer
      vim.api.nvim_create_autocmd('LspAttach', {
        group = vim.api.nvim_create_augroup('UserLspConfig', {}),
        callback = function(ev)
          local function map(mode, l, r, opts)
            opts = opts or {}
            opts.buffer = ev.buf
            vim.keymap.set(mode, l, r, opts)
          end
          -- Buffer local mappings.
          -- See `:help vim.lsp.*` for documentation on any of the below functions
          map('n', 'gD', vim.lsp.buf.declaration, { desc = 'LSP declaration' })
          -- map('n', 'gd', vim.lsp.buf.definition, { desc = 'LSP definition' })  -- Use Telescope
          -- map('n', 'gi', vim.lsp.buf.implementation, { desc = 'LSP implementation' })  -- Use Telescope
          -- map('n', 'gr', vim.lsp.buf.references, { desc = 'LSP reference' })  -- Use Telescope
          map('n', 'K', vim.lsp.buf.hover, { desc = 'Hover LSP info' })
          map('n', '<space>s', vim.lsp.buf.signature_help, { desc = 'Get LSP signature help' })
          map('n', '<space>wa', vim.lsp.buf.add_workspace_folder, { desc = 'LSP add workspace folder' })
          map('n', '<space>wr', vim.lsp.buf.remove_workspace_folder, { desc = 'LSP remove workspace folder' })
          map('n', '<space>wl', function()
            print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
          end, { desc = 'LSP list workspace folders' })
          -- map('n', '<space>D', vim.lsp.buf.type_definition, { desc = 'LSP type definition' })  -- Use Telescope
          map('n', '<space>rn', vim.lsp.buf.rename, { desc = 'Rename using LSP' })
          map({ 'n', 'v' }, '<space>c', vim.lsp.buf.code_action, { desc = 'LSP code action' })
          map('n', '<space>f', function()
            vim.lsp.buf.format { async = true }
          end, { desc = 'Format using LSP' })
        end,
      })

      -- Border style
      vim.diagnostic.config {
          float = { border = 'rounded' },
      }
      -- }}}
    end,
  },
  -- }}}

  -- echasnovski/mini.files {{{
  { 'echasnovski/mini.files',
    version = false,
    config = function()
      vim.keymap.set('n', '-', function()
        MiniFiles.open(vim.api.nvim_buf_get_name(0))
      end, { noremap = true, silent = true, desc = 'Open MiniFiles for current buffer' })

      require('mini.files').setup {
        -- Customization of explorer windows
        windows = {
          -- Whether to show preview of file/directory under cursor
          preview = true,
          -- Width of preview window
          width_preview = 50,
        },
      }
    end,
  },
  -- }}}

  -- otavioschwanck/arrow.nvim {{{
  { 'otavioschwanck/arrow.nvim',
    opts = {
      show_icons = true,
      leader_key = ';', -- Recommended to be a single key
      buffer_leader_key = 'm', -- Per Buffer Mappings
    }
  }
  -- }}}
})
