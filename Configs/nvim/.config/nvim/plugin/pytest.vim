if get(g:, 'loaded_pytest_plugin')
    finish
endif
let g:loaded_pytest_plugin = 1

if !executable('pytest')
    finish
endif

augroup pytest_plugin
  autocmd!
  " Old version that is no longer needed, because we don't use any custom
  " pytest launchers anymore.
  " au BufRead,BufNewFile *tests/test_*.py command! -buffer -bang -nargs=* Pytest :exe
  "       \ 'SlimeSend0 "' .
  "       \ (executable('iqe')         ? 'iqe tests custom -vvv -r a -s --pdb' :
  "       \ (executable('miq-runtest') ? 'miq-runtest -vvv -r a -s --runxfail --long-running --use-template-cache --pdb'
  "       \                            : 'pytest -vvv -r a -s --pdb')) . ' ' . <q-args> .
  "       \ (<bang>0 ? '\r"'
  "       \          : ' -k \"' . substitute(cfi#format("%s", ""), '\.', ' and ', '') . '\" ' . shellescape(expand('%:p')) . '\r"')


  " Run current test(s) in tmux pane. Depends on vim-slime and current-func-info.vim.
  " Expects to be launched inside test function or class or file.
  "
  " `:echo cfi#format("%s", "")` returns e.g.
  " `TestRewards.test_reward_addr_delegation._check_ledger_state`
  " and we want to get
  " `TestRewards and test_reward_addr_delegation`
  " The first `substitute` replaces `.` with ` and `, and the second `substitute` removes everything after the first `.`, i.e.
  " functions nested in test functions.
  " Test with
  " :echo substitute(substitute(cfi#format("%s", ""), '\.', ' and ', ''), '\..*$', '', '')
  au BufRead,BufNewFile *tests*/test_*.py command! -buffer -bang -nargs=* Pytest :exe
        \ 'SlimeSend0 "' . 'pytest -vvv -r a -s --pdb ' . <q-args> .
        \ (<bang>0 ? '\r"'
        \          : ' -k \"' . substitute(substitute(cfi#format("%s", ""), '\.', ' and ', ''), '\..*$', '', '') . '\" ' . shellescape(expand('%:p')) . '\r"')
augroup END
