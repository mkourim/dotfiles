if get(g:, 'loaded_projects_plugin')
    finish
endif
let g:loaded_projects_plugin = 1

let s:homedir = expand('~') . '/'
let s:reposdir = s:homedir . 'Source/repos/'

function! s:SetupEnvironment()
  let l:path = expand('%:p')

  if l:path =~ s:reposdir . 'iqe-'
    if &filetype == 'python'
      setlocal tw=100
    endif
  endif
endfunction

autocmd! BufReadPost,BufNewFile * call s:SetupEnvironment()
