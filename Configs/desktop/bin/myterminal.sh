#!/bin/sh

# export GTK_IM_MODULE=ibus
exec xfce4-terminal --maximize --hide-menubar --hide-borders --hide-toolbar --hide-scrollbar "$@"
