#!/bin/bash

bcklight_set() {
  dbus-send \
    --system \
    --type=method_call \
    --dest="org.freedesktop.UPower" \
    "/org/freedesktop/UPower/KbdBacklight" \
    "org.freedesktop.UPower.KbdBacklight.SetBrightness" \
    int32:"$1"
}

bcklight_get() {
  local bsetting
  bsetting="$(dbus-send \
    --system \
    --type=method_call \
    --print-reply=literal \
    --dest="org.freedesktop.UPower" \
    "/org/freedesktop/UPower/KbdBacklight" \
    "org.freedesktop.UPower.KbdBacklight.GetBrightness" |
    { read -r _ val; echo "$val"; }
  )"
  echo "$bsetting"
}

bsetting="$(bcklight_get)"
case "$bsetting" in
  0) bcklight_set 1 ;;
  1) bcklight_set 2 ;;
  2) bcklight_set 0 ;;
esac
