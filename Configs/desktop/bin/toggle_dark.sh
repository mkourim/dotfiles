#!/bin/sh

set -euo pipefail

THEME="Adwaita"

color_scheme="prefer-dark"
gtk3_theme="${THEME}-dark"
toggle=""

case "${1:-""}" in
  *light)
    color_scheme="prefer-light"
    gtk3_theme="$THEME"
    ;;
  *dark)
    ;;
  *default)
    color_scheme="default"
    gtk3_theme="$THEME"
    ;;
  *)
    toggle=true
    ;;
esac

if [[ -n "$toggle" && "$(gsettings get org.gnome.desktop.interface color-scheme)" == "'prefer-"* ]]; then
  color_scheme="default"
  gtk3_theme="$THEME"
fi

gsettings set org.gnome.desktop.interface color-scheme "$color_scheme"
gsettings set org.gnome.desktop.interface gtk-theme "$gtk3_theme"
