" Set the background to dark when the local time is between 20:00 and 06:00
" or month is between September and May.
if strftime('%H') >= 20 || strftime('%H') < 6 || strftime('%m') >= 9 || strftime('%m') < 5
  set background=dark
endif
