" Set the background to dark, the desktop is never on sunlight.
" Set it only when not connected over ssh (SSH_TTY is not set and DISPLAY doesn't contain localhost).
if empty($SSH_TTY) && $DISPLAY !~? 'localhost'
  set background=dark
" Or set it to dark when the local time is between 20:00 and 06:00
" or month is between September and May.
elseif strftime('%H') >= 20 || strftime('%H') < 6 || strftime('%m') >= 9 || strftime('%m') < 5
  set background=dark
endif

