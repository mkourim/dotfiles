#!/bin/sh

set -e

if [ -z "$SUDO_USER" ]; then
  echo "Must run using sudo" >&2
  exit 1
fi

NUM="${1:-0}"

VIRBR="virbr0"

if [ ! -e "/etc/netns/vnet${NUM}" ]; then
  mkdir -p "/etc/netns/vnet${NUM}"
  printf "nameserver 8.8.8.8\nsearch .\n" > "/etc/netns/vnet${NUM}/resolv.conf"
fi

if ! ip link show "hveth${NUM}" >/dev/null 2>&1; then
  virbr0_route="$(ip route show dev "$VIRBR" | sed -n 's;.* src \([^ ]*\) .*;\1;p')"
  virbr0_pref="${virbr0_route%.*}"

  ip link add name "hveth${NUM}" type veth peer name "cveth${NUM}"
  ip netns add "vnet${NUM}"
  ip link set "cveth${NUM}" netns "vnet${NUM}"
  ip link set "hveth${NUM}" master "$VIRBR" up
  ip -n "vnet${NUM}" link set lo up
  ip -n "vnet${NUM}" addr add "${virbr0_pref}.$((156 + NUM))/24" dev "cveth${NUM}"
  ip -n "vnet${NUM}" link set dev "cveth${NUM}" up
  ip -n "vnet${NUM}" route add default via "$virbr0_route"
  sleep 3
fi

ip netns exec "vnet${NUM}" ping -c2 8.8.8.8 && printf '\nOK! Entering vnet%s namespace!\n' "$NUM"
ip netns exec "vnet${NUM}" su -l --whitelist-environment=DISPLAY "$SUDO_USER"
