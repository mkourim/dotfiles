#!/bin/sh

[ -z "$VIRTUAL_ENV" ] && { echo "No virtualenv" >&2; exit 1; }
pip list --outdated --format=freeze | cut -d = -f 1 | xargs -n1 pip install -U
