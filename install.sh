#!/usr/bin/env bash

# Install the dotfiles.
# To reinstall individual config groups, run e.g. `stow -d Configs -t ~/ -R bash bat`
# NOTE: fonts need to be reinstalled explicitly to avoid breaking the terminal.

set -euo pipefail

if ! command -v stow >/dev/null 2>&1; then
  echo "stow is not installed" >&2
  exit 1
fi

create_dirs() {
  local dirs=(
    "$HOME/bin"
    "$HOME/.elinks"
    "$HOME/.tmuxp"
    "$HOME/.config/nvim"
    "$HOME/.config/bat"
    "$HOME/.config/nix"
    "$HOME/.config/nixpkgs"
    "$HOME/.config/tmux"
  )
  if [ "${1:-}" = "container" ]; then
    dirs+=(
      "$HOME/.config/mypy"
      "$HOME/.config/yamllint"
    )
  else
    dirs+=(
      "$HOME/.vim"
      "$HOME/.config/alacritty"
      "$HOME/.config/systemd"
      "$HOME/.config/user-tmpfiles.d"
      "$HOME/.config/xfce4/terminal/colorschemes"
      "$HOME/.local/share/applications"
      "$HOME/.local/share/fonts"
    )
  fi
  mkdir -p "${dirs[@]}"
}

if [ -d "Configs/00-untracked" ]; then
  untracked_dir="00-untracked"
else
  untracked_dir=""
fi

# Check if we are re-installing the symlinks, or installing for the first time.
fonts_dir="fonts"
stow_args=()
if [ "$(readlink -m "$HOME/.bashrc")" = "$(readlink -m Configs/bash/.bashrc)" ]; then
  # Install fonts only when not already installed,
  # otherwise symlink change can break the terminal.
  fonts_dir=""
  stow_args=("-R")
fi

stow_configs() {
  local configs=(
    bash
    bat
    fzf
    gem
    ghc
    git
    nix
    ripgrep
    scripts
    tmux
    tmuxp
    yamllint
    "$untracked_dir"
  )
  if [ "${1:-}" = "container" ]; then
    configs+=(
      elinks
      git-container
      mld
      mypy
      nvim
      nvim-dark-dynamic-ssh
    )
  else
    configs+=(
      calibre
      cheat
      desktop
      docker
      git-host
      systemd
      vim-dark-dynamic
      vim-simple
      xfce4-terminal-color
      xorg
      "$fonts_dir"
    )
  fi
  stow -d Configs -t "$HOME" "${stow_args[@]}" "${configs[@]}"
}

if [ -e /run/.containerenv ]; then
  echo "Running installation in a container"
  create_dirs container
  stow_configs container
else
  echo "Running installation on a host"
  create_dirs
  stow_configs
fi
